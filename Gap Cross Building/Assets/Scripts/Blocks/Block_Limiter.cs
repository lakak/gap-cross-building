﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Block_Limiter : MonoBehaviour
{
    public float maxStone;
    public Text stoneLimitText;
    private float persistentmaxStone;

    public float maxBridge;
    public Text bridgeLimitText;
    private float persistentmaxBridge;

    public float maxHookLoop;
    public Text hookLoopLimitText;
    private float persistentmaxHookLoop;

    // Start is called before the first frame update
    void Start()
    {
        persistentmaxStone = maxStone;
        persistentmaxBridge = maxBridge;
        persistentmaxHookLoop = maxHookLoop;
        GUIUpdate();
    }
    
    public float BlockPlace(float number)
    {
        float result;
        result = number;
        if (result == 1)
        {
            --maxStone;
            stoneLimitText.text = maxStone.ToString();
        }

        if (result == 2)
        {
            --maxBridge;
            bridgeLimitText.text = maxBridge.ToString();
        }

        if(result == 3)
        {
            --maxHookLoop;
            hookLoopLimitText.text = maxHookLoop.ToString();
        }

        return result;
    }

    public float BlockRegain(float number)
    {
        float result;
        result = number;

        if (result == 1)
        {
            ++maxStone;
            if (maxStone >= persistentmaxStone)
            {
                maxStone = persistentmaxStone;
            }
            stoneLimitText.text = maxStone.ToString();
        }

        if (result == 2)
        {
            ++maxBridge;
            if (maxBridge >= persistentmaxBridge)
            {
                maxBridge = persistentmaxBridge;
            }
            bridgeLimitText.text = maxBridge.ToString();
        }

        if (result == 3)
        {
            ++maxHookLoop;
            if (maxHookLoop >= persistentmaxHookLoop)
            {
                maxHookLoop = persistentmaxHookLoop;
            }
            hookLoopLimitText.text = maxHookLoop.ToString();
        }

        return result;
    }
    public void BlockReset()
    {
        maxStone = persistentmaxStone;
        maxBridge = persistentmaxBridge;
        maxHookLoop = persistentmaxHookLoop;
        GUIUpdate();
    }

    public void GUIUpdate()
    {
        stoneLimitText.text = maxStone.ToString();
        bridgeLimitText.text = maxBridge.ToString();
        hookLoopLimitText.text = maxHookLoop.ToString();
    }

 
}
