﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookLoop_Block_Script : MonoBehaviour
{
    private bool shouldFall;
    public Rigidbody rigBod;
    public BoxCollider collide;

    // Start is called before the first frame update
    void Start()
    {
        rigBod = GetComponent<Rigidbody>();
        collide = GetComponent<BoxCollider>();
        //DebugDraw();
        OtherCheck();

    }

    private void OnDrawGizmos()
    {
        DebugDraw();
    }

    void Update()
    {
        OtherCheck();
    }

    public void OtherCheck()
    {
        if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward) * 1, 1, 1, QueryTriggerInteraction.Ignore))
        {
            if (!Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.forward) * 1, 1, 1, QueryTriggerInteraction.Ignore))
            {
                if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right) * 1, 1, 1, QueryTriggerInteraction.Ignore))
                {
                    if (!Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.right) * 1, 1, 1, QueryTriggerInteraction.Ignore))
                    {

                    }

                    else
                    {
                        Fall();
                    }
                }
                else
                {
                    Fall();
                }
            }
            else
            {
                Fall();
            }
        }
        else
        {
            Fall();
        }
    }

    void Fall()
    {
        rigBod.useGravity = true;
        rigBod.constraints = RigidbodyConstraints.None;
        collide.center = new Vector3(0, 0.1f, 0);
        collide.size = new Vector3(0.95f, 0.15f, 0.95f);
        //collide.enabled = false;
    }
    public void DebugDraw()
    {
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1, Color.blue, 0.1f);
        Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.forward) * 1, Color.blue, 0.1f);
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right) * 1, Color.blue, 0.1f);
        Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.right) * 1, Color.blue, 0.1f);
    }
}