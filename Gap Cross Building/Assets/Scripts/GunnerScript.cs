﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GunnerScript : MonoBehaviour
{
    public bool friendly = false;
    public float range = 8;
    public GameObject visual;
    public CharacterController player;
    public GameObject shootLight;

    public AudioClip shootSound;
    public AudioClip blockedSound;
    public AudioClip friendSound;

    public AudioSource soundSource;

    public float lastState;

    public float killDelay = 0.75f;

    public string sceneToGoTo;

    public GameObject traceLocation;

    void Start()
    {
        soundSource = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        DebugDraw();
        Shoot();
    }

    void Shoot()
    {
        Vector3 fwd = (transform.TransformDirection(Vector3.forward));
        Ray gunfire = new Ray(traceLocation.transform.position, fwd);
        RaycastHit hit;
        if (Physics.Raycast(gunfire, out hit, range, 1, QueryTriggerInteraction.Ignore))
        {
            if (hit.collider.CompareTag("Player"))
            {
                if (lastState != 1)
                {
                    if (friendly == true)
                    {
                        soundSource.clip = friendSound;
                        soundSource.Play();
                        visual.SetActive(true);
                        lastState = 1;
                    }
                    else
                    {
                        visual.SetActive(true);
                        player.GetComponent<CharacterController>().enabled = false;
                        soundSource.clip = shootSound;
                        soundSource.Play();
                        lastState = 1;
                        Invoke("Kill", killDelay);
                    }

                }
            }
            else
            {
                if (!hit.collider.CompareTag("Block_Bridge"))
                {
                    if (lastState != 2)
                    {
                        soundSource.clip = blockedSound;
                        soundSource.Play();
                        visual.SetActive(false);
                        lastState = 2;
                    }
                }
            }
        }
        else
        {
            if (lastState != 0)
            {
                visual.SetActive(true);
                lastState = 0;
            }
        }
    }

    void Kill()
    {
        shootLight.SetActive(true);
        SceneManager.LoadScene(sceneName: sceneToGoTo);
    }
    void DebugDraw()
    {
        Vector3 fwd = (transform.TransformDirection(Vector3.forward) * range);
        Debug.DrawRay(traceLocation.transform.position, fwd, Color.blue, 0.1f);
    }

    private void OnDrawGizmos()
    {
        DebugDraw();
        //Shoot();
    }
}
