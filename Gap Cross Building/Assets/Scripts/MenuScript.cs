﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public bool pause = false;
    public bool playerEnd = false;
    public GameObject menuToClose;

    public void OpenLevel(string scene)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(scene);
    }

    public void URLOpen(string url)
    {
        Application.OpenURL(url);
    }

    public void Quit()
    {
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }

    public void Resume()
    {
        Time.timeScale = 1;
        menuToClose.SetActive(false);
        //hide the curosor
        Cursor.visible = false;
        //keep the cursor locked in place so that it doesnt go off screen
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (playerEnd == true)
            {
                menuToClose.SetActive(true);
                //hide the curosor
                Cursor.visible = true;
                //keep the cursor locked in place so that it doesnt go off screen
                Cursor.lockState = CursorLockMode.None;
                Time.timeScale = 0;
            }
        }
    }
}
