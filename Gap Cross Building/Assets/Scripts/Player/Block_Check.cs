﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_Check : MonoBehaviour
{
    public GameObject lastHit;
    public float detectionLength = 4f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //print(lastHit);
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * detectionLength, Color.blue, 0.1f);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, fwd, out hit, detectionLength))
        {
            if (lastHit ==null)
            {
                //print("Help!");
                if (hit.collider.tag == "Visual_Block_Representation")
                {
                    lastHit = hit.collider.gameObject;
                }

                if (hit.collider.tag == "Block_HookAndLoop_Detection")
                {
                    lastHit = hit.collider.gameObject;
                }
            }

            else
            {
                if (hit.collider.tag == "Visual_Block_Representation")
                {
                    if (hit.collider.gameObject == lastHit)
                    {
                        lastHit = hit.collider.gameObject;
                        //print("Visual Found, was last hit already");
                        hit.collider.GetComponent<MeshRenderer>().enabled = true;
                        //print("showing");
                    }

                    else
                    {
                        lastHit.GetComponent<MeshRenderer>().enabled = false;
                        lastHit = hit.collider.gameObject;
                    }
                }
                else
                {
                    lastHit.GetComponent<MeshRenderer>().enabled = false;

                    if (hit.collider.tag == "Block_HookAndLoop_Detection")
                    {
                        if (hit.collider.gameObject == lastHit)
                        {
                            lastHit = hit.collider.gameObject;
                            hit.collider.GetComponent<MeshRenderer>().enabled = true;
                        }

                        else
                        {
                            lastHit.GetComponent<MeshRenderer>().enabled = false;
                            lastHit = hit.collider.gameObject;
                        }
                    }
                    else
                    {
                        lastHit.GetComponent<MeshRenderer>().enabled = false;
                    }

                }
            }

        }

        else
        {
            if (lastHit != null)
            {
                lastHit.GetComponent<MeshRenderer>().enabled = false;
            }

        }

        //print(lastHit);
    }
}
