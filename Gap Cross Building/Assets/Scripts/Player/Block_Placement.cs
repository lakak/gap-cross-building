﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_Placement : MonoBehaviour
{
    public float detectionLength = 4f;
    public Transform lastHitTransform;
    public Block_Limiter blockLimiter;

    public AudioClip stoneSound;
    public AudioClip bridgeSound;
    public AudioClip hookLoopSound;
    public AudioClip hookLoopBreakSound;

    public AudioSource soundSource;

    public Camera cameraToUse;

    private bool shouldPlace;

    public float selectedBlock = 1;

    public MeshRenderer stoneViewmodel;
    public MeshRenderer bridgeViewmodel;
    public MeshRenderer hookLoopViewmodel;

    public GameObject stonePrefab;
    public GameObject bridgePrefab;
    public GameObject hookLoopPrefab;

    public Animator stoneAnimator;
    public Animator bridgeAnimator;
    public Animator hookLoopAnimator;
 

    // Start is called before the first frame update
    void Start()
    {
        blockLimiter = FindObjectOfType<Block_Limiter>();
        soundSource = GetComponent<AudioSource>();
        ViewmodelUpdate();
    }

    void ViewmodelUpdate()
    {
        switch (selectedBlock)
        {
            default:
                print("Default selectedBlock");
                break;
            case 1:
                stoneViewmodel.enabled = true;
                bridgeViewmodel.enabled = false;
                hookLoopViewmodel.enabled = false;
                break;
            case 2:
                stoneViewmodel.enabled = false;
                bridgeViewmodel.enabled = true;
                hookLoopViewmodel.enabled = false;
                break;
            case 3:
                stoneViewmodel.enabled = false;
                bridgeViewmodel.enabled = false;
                hookLoopViewmodel.enabled = true;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            PlaceBlock();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            selectedBlock = 1;
            ViewmodelUpdate();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            selectedBlock = 2;
            ViewmodelUpdate();
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            selectedBlock = 3;
            ViewmodelUpdate();
        }
    }

    void SoundPlay(float number)
    {
        switch (number)
        {
            default:
                break;
            case 1:
                soundSource.clip = stoneSound;
                soundSource.Play();
                break;
            case 2:
                soundSource.clip = bridgeSound;
                soundSource.Play();
                break;
             case 3:
                soundSource.clip = hookLoopSound;
                soundSource.Play();
                break;
             case 4:
                soundSource.clip = hookLoopBreakSound;
                soundSource.Play();
                break;
        }
    }

    void PlaceBlock()
    {
        Debug.DrawRay(cameraToUse.transform.position, cameraToUse.transform.TransformDirection(Vector3.forward) * detectionLength, Color.white, 5);
        RaycastHit hit;

        if (Physics.Raycast(cameraToUse.transform.position, cameraToUse.transform.forward, out hit, detectionLength))
        {
            if (!hit.collider.CompareTag ("Visual_Block_Representation"))
            {
                if (hit.collider.CompareTag ("Block_HookAndLoop_Detection"))
                {
                    Destroy(hit.collider.transform.parent.gameObject);
                    SoundPlay(4);
                    blockLimiter.BlockRegain(3);
                    hookLoopAnimator.SetTrigger("Reuse");
                }
                shouldPlace = false;
            }
            else
            {
                shouldPlace = true;
            }
            if (hit.collider.CompareTag ("Visual_Block_Representation"))
                lastHitTransform = hit.collider.transform;

            if (shouldPlace == true)
            {
                switch (selectedBlock)
                {
                    default:
                        break;
                    case 1:
                        if (blockLimiter.maxStone == 0)
                        {

                        }
                        else
                        {
                            Instantiate(stonePrefab, lastHitTransform.transform.position, Quaternion.identity);
                            blockLimiter.BlockPlace(1);
                            SoundPlay(1);
                            //stoneAnimator.ResetTrigger("DestroyAll");
                            stoneAnimator.SetTrigger("Build");
                        }
                        break;
                    case 2:

                        if (blockLimiter.maxBridge == 0)
                        {

                        }
                        else
                        {
                            Instantiate(bridgePrefab, lastHitTransform.transform.position, Quaternion.identity);
                            blockLimiter.BlockPlace(2);
                            SoundPlay(2);
                            bridgeAnimator.SetTrigger("Build");
                        }
                        break;
                    case 3:
                        if (blockLimiter.maxHookLoop == 0)
                        {

                        }
                        else
                        {
                            Instantiate(hookLoopPrefab, lastHitTransform.transform.position, Quaternion.identity);
                            blockLimiter.BlockPlace(3);
                            SoundPlay(3);
                            hookLoopAnimator.SetTrigger("Build");
                        }
                        break;
                }

            }

        }
    }
}
 

    