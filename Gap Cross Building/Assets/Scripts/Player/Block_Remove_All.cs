﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_Remove_All : MonoBehaviour
{
    private GameObject[] allBlocks;
    public Block_Limiter limiter;
    public AudioSource soundSource;
    public AudioClip breakSound;

    public Animator stoneAnimator;
    public Animator bridgeAnimator;
    public Animator hookLoopAnimator;

    // Start is called before the first frame update
    void Start()
    {
        limiter = FindObjectOfType<Block_Limiter>();
        soundSource = GetComponent<AudioSource>();
        soundSource.clip = breakSound;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            allBlocks = GameObject.FindGameObjectsWithTag("Block_Stone");
            for(var i = 0; i < allBlocks.Length; i ++)
            {
                Destroy(allBlocks[i]);
                limiter.BlockReset();
                soundSource.Play();
                //stoneAnimator.ResetTrigger("Build");
                stoneAnimator.SetTrigger("DestroyAll");
            }

            allBlocks = GameObject.FindGameObjectsWithTag("Block_Bridge");
            for (var i = 0; i < allBlocks.Length; i++)
            {
                Destroy(allBlocks[i]);
                limiter.BlockReset();
                soundSource.Play();
                bridgeAnimator.SetTrigger("DestroyAll");
            }

            allBlocks = GameObject.FindGameObjectsWithTag("Block_HookAndLoop");
            for (var i = 0; i < allBlocks.Length; i++)
            {
                Destroy(allBlocks[i]);
                limiter.BlockReset();
                soundSource.Play();
                hookLoopAnimator.SetTrigger("DestroyAll");
            }


        }
    }
}
