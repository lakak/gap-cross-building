﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Look_Mouse : MonoBehaviour
{
    public Transform cameraToUse;
    public Transform player;

    public float horizontalSensitivity = 2000;
    public float verticalSensitivity = 2000;

    private float pitch = 0;
    public float viewClamp = 90;

    // Start is called before the first frame update
    void Start()
    {
        //hide the curosor
        Cursor.visible = false;
        //keep the cursor locked in place so that it doesnt go off screen
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        //move the camera and player horizontally
        float mouseX = Input.GetAxis("Mouse X");
        float rotateX = mouseX * (horizontalSensitivity * Time.deltaTime);
        player.Rotate(0, rotateX, 0);

        //move the camera and player vertically
        float mouseY = Input.GetAxis("Mouse Y");
        float rotateY = mouseY * (verticalSensitivity * Time.deltaTime);
        cameraToUse.Rotate(-rotateY, 0, 0);

        //prevent the camera from going out of clamp range
        pitch = Mathf.Clamp(pitch + rotateY, -viewClamp, viewClamp);

        cameraToUse.rotation = Quaternion.Euler(-pitch, player.rotation.eulerAngles.y, 0);
    }
}
