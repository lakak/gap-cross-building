﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Move : MonoBehaviour
{
    private CharacterController charController;
    public float speed = 3f;
    public float gravity = 5f;
    public float jumpAmount = 16;

    private Vector3 moveDirection = Vector3.zero;
    private Vector3 charactervelocity = (Vector3.zero);

    // Start is called before the first frame update
    void Start()
    {
        //gets and sets the character controller
        charController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        DoInput();

        //this needs to be in update otherwise movement speed acts weird
        charController.Move(charactervelocity);

        //scrapped jumping is here, breaks the game kinda so it was removed.
        //if (Input.GetKeyDown(KeyCode.Space))
        {
            //charController.Move(transform.up * jumpAmount);
            //moveDirection.y = jumpAmount;
        }

        charController.Move(moveDirection * Time.deltaTime);
        if (charController.isGrounded)
        {
            moveDirection.y = -1;
        }
        else
        {
            charController.SimpleMove(Vector3.forward * 0 * Time.deltaTime);
            moveDirection.y -= gravity * 10 * Time.deltaTime;
        }
    }

    private void FixedUpdate()
    {

    }

    void DoInput()
    {
        charactervelocity = GetInputDirection() * (speed * Time.deltaTime);
    }

    Vector3 GetInputDirection()
    {
        Vector3 direction = Vector3.zero;
        direction = (Input.GetAxis("Vertical") * transform.forward) + (Input.GetAxis("Horizontal") * transform.right);
        return direction;
    }
}
