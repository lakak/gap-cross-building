﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trig_Destroy : MonoBehaviour
{
    public Block_Limiter blockLimiter;
    public GameObject lastOverlapped;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Block_Stone"))
        {
            if (other.gameObject == lastOverlapped)
            {
                Destroy(other.gameObject);

            }
            else
            {
                Destroy(other.gameObject);
                blockLimiter.BlockRegain(1);
                lastOverlapped = other.gameObject;
            }

        }

        if (other.gameObject.CompareTag("Block_Bridge"))
        {
            if (other.gameObject == lastOverlapped)
            {
                Destroy(other.gameObject);

            }
            else
            {
                Destroy(other.gameObject);
                blockLimiter.BlockRegain(2);
                lastOverlapped = other.gameObject;
            }
        }

        if (other.gameObject.CompareTag("Block_HookAndLoop"))
        {
            if (other.gameObject == lastOverlapped)
            {
                Destroy(other.gameObject);

            }
            else
            {
                Destroy(other.gameObject);
                blockLimiter.BlockRegain(3);
                lastOverlapped = other.gameObject;
            }
        }
    }
}
