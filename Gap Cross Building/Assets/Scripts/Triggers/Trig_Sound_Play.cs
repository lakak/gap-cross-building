﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Trig_Sound_Play : MonoBehaviour
{
    public string sceneToGoTo;
    public AudioSource source;
    public bool playOnce = true;
    private bool hasPlayed = false;

    public AudioClip soundToPlay;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (playOnce == true)
        {
            if (hasPlayed == false) 
            {
                source.clip = soundToPlay;
                source.Play();
                hasPlayed = true;
            }
        }
        else
        {
            source.clip = soundToPlay;
            source.Play();
        }

    }
}
